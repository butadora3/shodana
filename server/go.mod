module server

require (
	github.com/gin-contrib/cors v0.0.0-20190101123304-5e7acb10687f
	github.com/gin-gonic/contrib v0.0.0-20181101072842-54170a7b0b4b
	github.com/gin-gonic/gin v1.3.0
	github.com/jlaffaye/ftp v0.0.0-20190126081051-8019e6774408
	github.com/mattn/go-sqlite3 v1.10.0
)
