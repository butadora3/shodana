package handler

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/gin-gonic/gin"
	_ "github.com/mattn/go-sqlite3"
)

type File struct {
	Path string `json:"path"`
	Size int64  `json:"size"`
}

// Function for test
func ForTest(c *gin.Context) {
	// データベースのコネクションを開く
	db, err := sql.Open("sqlite3", "./test.db")
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusNotFound, gin.H{"message": err.Error()})
		return
	}

	// 複数レコード取得
	rows, err := db.Query(
		`SELECT * FROM files`,
	)
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusNotFound, gin.H{"message": err.Error()})
		return
	}

	// 処理が終わったらカーソルを閉じる
	defer rows.Close()
	var files []File
	for rows.Next() {
		var id int64
		var path string
		var tag string
		var filename string
		var year int

		// カーソルから値を取得
		if err := rows.Scan(&id, &path, &tag, &filename, &year); err != nil {
			log.Fatal("rows.Scan()", err)
			return
		}

		f := File{
			Path: path + "/" + filename,
			Size: id,
		}
		files = append(files, f)
	}
	files = files[1:]

	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusNotFound, gin.H{"message": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"message": files,
	})
}

func dirwalk(dir string) (files []File, err error) {
	err = filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if !strings.HasPrefix(info.Name(), ".") {
			return nil
		}
		path = strings.Replace(path, "files/", "http://localhost:8888/", 1)
		size := info.Size()
		f := File{
			Path: path,
			Size: size,
		}
		files = append(files, f)
		return nil
	})
	if err != nil {
		return
	}
	files = files[1:]
	return
}

// List return url & size list
func List(c *gin.Context) {
	files, err := dirwalk("./files")
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusNotFound, gin.H{"message": err.Error()})
		return
	}
	c.JSON(http.StatusOK, files)
}

// Upload upload files.
func Upload(c *gin.Context) {
	form, _ := c.MultipartForm()
	files := form.File["file"]

	uuid := c.PostForm("uuid")

	fileCount := len(files)

	for _, file := range files {
		if file.Header.Get("Content-Type") != "application/pdf" {
			fileCount--
			continue
		}
		err := c.SaveUploadedFile(file, "files/"+uuid+".pdf")
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
		}
	}
	if fileCount == len(files) {
		c.String(http.StatusOK, fmt.Sprintf("%d files uploaded!", fileCount))
	} else {
		c.String(http.StatusBadRequest, fmt.Sprintf("%d/%d files uploaded!", fileCount, len(files)))
	}
}

func Delete(c *gin.Context) {
	uuid := c.Param("uuid")
	err := os.Remove(fmt.Sprintf("files/%s.pdf", uuid))
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"message": fmt.Sprintf("id: %s is deleted!", uuid)})
}
